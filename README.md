## Task
Develop a simple flask app which reads Nmap data and visualizes them in a simple way.

## How to get it running
* Requirements: Python, Flask
* Run `python app.py`
* Visit port to view app

## Final Checks

[ ] Proper folder structure

[ ] Refactor code

[ ] Tests?