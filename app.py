from flask import Flask, render_template
import os
import requests
import json

app = Flask(__name__)

@app.route("/")
def showjson():
  basedir = os.path.abspath(os.path.dirname(__file__))
  
  fileNames = os.listdir("nmap-data")
  print fileNames
  
  json_url = os.path.join(basedir, "nmap-data", fileNames[1])
  data = json.load(open(json_url))
  return render_template("home.html", data=data)

if __name__ == "__main__":
  app.run(debug=True)